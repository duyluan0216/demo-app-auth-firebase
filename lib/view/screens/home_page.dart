import 'package:auth_firebase_2/providers/auth_view_provider.dart';
import 'package:auth_firebase_2/view/screens/base_view.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  AppBar _buildAppBar() {
    return AppBar(
      title: const Text('Trang chủ'),
    );
  }

  @override
  Widget build(BuildContext context) {
    var appBar = _buildAppBar();
    return BaseView<AuthViewProvider>(
      builder: (context, model, __) => Scaffold(
        appBar: appBar,
        body: SizedBox(
          width:
              MediaQuery.of(context).size.height - appBar.preferredSize.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Đây là email: ${model.currentUser!.email}'),
              Text('Đây là uid: ${model.currentUser!.uid}'),
              TextButton(
                child: const Text("Đăng xuất"),
                onPressed: () {
                  model.logOut();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
