import 'package:auth_firebase_2/providers/auth_view_provider.dart';
import 'package:auth_firebase_2/providers/base_provider.dart';
import 'package:auth_firebase_2/utils/enums/app_state.dart';
import 'package:flutter/material.dart';

class AuthStateProvider extends BaseProvider {
  void switchAuthenticationState(AuthViewProvider authViewProvider) {
    authViewProvider.authState == AuthState.signin
        ? authViewProvider.setAuthState(AuthState.signup)
        : authViewProvider.setAuthState(AuthState.signin);
  }

  void switchAuthenticationMethod(
    AuthViewProvider authViewProvider,
    TextEditingController emailController,
    TextEditingController passwordController,
  ) {
    authViewProvider.authState == AuthState.signin
        ? authViewProvider.signIn(
            emailController.text,
            passwordController.text,
          )
        : authViewProvider.createNewUser(
            emailController.text,
            passwordController.text,
          );
  }

  String switchAuthenticationText(AuthViewProvider authViewProvider) {
    return authViewProvider.authState == AuthState.signin
        ? "Đăng nhập"
        : "Đăng ký";
  }

  String switchAuthenticationOption(AuthViewProvider authViewProvider) {
    return authViewProvider.authState == AuthState.signin
        ? "Tạo tài khoản"
        : "Đăng nhập";
  }
}
