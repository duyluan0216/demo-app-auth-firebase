import 'package:auth_firebase_2/providers/base_provider.dart';
import 'package:auth_firebase_2/utils/enums/app_state.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthViewProvider extends BaseProvider {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  User? get currentUser => _firebaseAuth.currentUser;

  void createNewUser(String email, String password) async {
    setViewState(ViewState.loading);
    await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    setViewState(ViewState.done);
  }

  void signIn(String email, String password) async {
    setViewState(ViewState.loading);
    await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    setViewState(ViewState.done);
  }

  void logOut() async {
    setViewState(ViewState.loading);
    await _firebaseAuth.signOut();
    setViewState(ViewState.done);
  }
}
