import 'package:auth_firebase_2/utils/enums/app_state.dart';
import 'package:flutter/material.dart';

class BaseProvider extends ChangeNotifier {
  ViewState _viewState = ViewState.done;

  ViewState get viewState => _viewState;

  setViewState(ViewState viewState) {
    _viewState = viewState;
    notifyListeners();
  }

  AuthState _authState = AuthState.signin;

  AuthState get authState => _authState;

  setAuthState(AuthState authState) {
    _authState = authState;
    notifyListeners();
  }
}
