import 'package:auth_firebase_2/providers/auth_state_provider.dart';
import 'package:auth_firebase_2/providers/auth_view_provider.dart';
import 'package:auth_firebase_2/utils/enums/app_state.dart';
import 'package:auth_firebase_2/view/screens/base_view.dart';
import 'package:flutter/material.dart';

class AuthPage extends StatelessWidget {
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final AuthViewProvider authViewProvider;

  AuthPage({
    required this.emailController,
    required this.passwordController,
    required this.authViewProvider,
  });

  @override
  Widget build(BuildContext context) {
    return BaseView<AuthStateProvider>(
        builder: (context, authStateProvider, __) {
      return Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Center(
              child: Column(
                children: [
                  const SizedBox(
                    height: 200,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Nhập email',
                      fillColor: Colors.grey[100],
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(width: 0, color: Colors.grey),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(width: 0, color: Colors.grey),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                    controller: emailController,
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      labelText: 'Nhập password',
                      fillColor: Colors.grey[100],
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(width: 0, color: Colors.grey),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(width: 0, color: Colors.grey),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  authViewProvider.viewState == ViewState.loading
                      ? const CircularProgressIndicator()
                      : ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                          ),
                          child: Text(
                            authStateProvider
                                .switchAuthenticationText(authViewProvider),
                          ),
                          onPressed: () {
                            authStateProvider.switchAuthenticationMethod(
                                authViewProvider,
                                emailController,
                                passwordController);
                          }),
                  InkWell(
                    onTap: () {
                      authStateProvider
                          .switchAuthenticationState(authViewProvider);
                    },
                    child: Text(authStateProvider
                        .switchAuthenticationOption(authViewProvider)),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
