import 'package:auth_firebase_2/locator.dart';
import 'package:auth_firebase_2/providers/base_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BaseView<T extends BaseProvider> extends StatefulWidget {
  final Widget Function(BuildContext context, T model, Widget? child) builder;

  BaseView({
    required this.builder,
  });

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseProvider> extends State<BaseView<T>> {
  T model = getIt<T>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>.value(
      //builder: (context) => model,
      value: model,
      child: Consumer<T>(builder: widget.builder),
      //notifier: model,
    );
  }
}
