import 'package:auth_firebase_2/providers/auth_state_provider.dart';
import 'package:auth_firebase_2/providers/auth_view_provider.dart';
import 'package:auth_firebase_2/providers/base_provider.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => BaseProvider());
  getIt.registerLazySingleton(() => AuthViewProvider());
  getIt.registerLazySingleton(() => AuthStateProvider());
}
