import 'package:auth_firebase_2/providers/auth_view_provider.dart';
import 'package:auth_firebase_2/view/screens/auth_page.dart';
import 'package:auth_firebase_2/view/screens/base_view.dart';
import 'package:auth_firebase_2/view/screens/home_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class IndexView extends StatelessWidget {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  IndexView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseView<AuthViewProvider>(
      builder: (context, authViewProvider, child) => StreamBuilder(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            return snapshot.hasData
                ? HomePage()
                : AuthPage(
                    emailController: emailController,
                    passwordController: passwordController,
                    authViewProvider: authViewProvider,
                  );
          }),
    );
  }
}
